# FFXI Noesis Files

This repo contains all my Noesis files for character-related assets.

They use absolute paths to "C:\Program Files (x86)\PlayOnline\SquareEnix\FINAL FANTASY XI\", so if this does not match your setup, you will either need to change it or use the registry option (which may or may not work - didn't for me on Windows). If you're a Linux user or have access to a Linux shell, you can use `sed -i 's#find#replace#g' <file or * for all files in folder>` to quickly change the directories.

To change what piece of equipment a character is wearing, change the DAT file being pointed to. These files don't include weapons but you can add them - have a look at the sample files in the Noesis scenes directory.

If you have any questions, feel free to reach out, and if there are any improvements that can be made, contact me or submit a merge request directly. :)
